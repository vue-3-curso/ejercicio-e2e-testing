// https://docs.cypress.io/api/table-of-contents

describe('Login Test Feature', () => {
  it('User can not access to protected router when is not logged in', () => {
    cy.visit('/')
    cy.url().should('eq','http://localhost:8081/login')
  }),

  it('User with wrong credentials can not pass',()=>{
    cy.login('user@user.com','abc123')
    cy.url().should('eq','http://localhost:8081/login')
  }),

  it('User can succesfully login',()=>{
    cy.login('admin@admin.com','12345678')
    cy.url().should('eq','http://localhost:8081/')
  })
})
