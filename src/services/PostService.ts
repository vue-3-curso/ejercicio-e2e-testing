import { ref,Ref } from 'vue'
import IPost from './interfaces/IPost'

class PostService{
    private post:IPost
    private posts:Array<IPost>

    constructor(){
        this.post={}
        this.posts=[]
    }

    getPost():IPost{
        return this.post
    }

    getPosts():Array<IPost>{
        return this.posts
    }

    async fetchAll():Promise<void>{
        try{
            const url='https://jsonplaceholder.typicode.com/posts'
            const response=await fetch(url)
            const json=await response.json()
            this.posts=await json
        }catch(error){
            console.log(error);
        }
    }

    async fetchById(id:string | Array<string>):Promise <void>{
        try {
            const url=`https://jsonplaceholder.typicode.com/posts/${id}`
            const response=await fetch(url)
            const json=await response.json()
            this.post=await json
        } catch (error) {
            console.log(error);
        }
    }
}

export default PostService